#include <stdio.h>
#include <stdlib.h>

#define MIN_ARGS 2

int print_help();
char get_random_char();

int main(int argc, char **argv) {


    //checking if argument has been provided
    if(argc < MIN_ARGS) {
        print_help();
        exit(1);
    }

    int password_length = atoi(argv[argc - 1]);

    if(password_length <= 0) {
        fprintf(stderr, "Must provide a natural number as length\n\n");
        print_help();
        exit(1);
    }

    //check if argument is int

    //creating a long buffer size and populating it with getrandom syscall
    unsigned long int *seed = malloc(sizeof(unsigned long int));
    if(!seed) {
        fprintf(stderr, "unable to allocate buffer size space. exiting\n");
        exit(1);
    }
    getrandom(seed, sizeof(unsigned long int), 0);

    //initializing random pool with buffer
    srandom(*seed);
    free(seed);

    // creating string buffer and populating it with random ASII char

    size_t buffer_size = password_length;
    char *string_buffer = malloc(sizeof(char) * buffer_size);
    for(int i = 0; i < buffer_size; i ++) {
        string_buffer[i] = get_random_char();
    }

    //print the random string
    fwrite(string_buffer, buffer_size, 1, stdout);
    printf("\n");
    //free string buffer
    free(string_buffer);
    
    return EXIT_SUCCESS;
}

int print_help() {
        fprintf(stderr, "\n");
        fprintf(stderr, "Usage:  passwordgen [OPTIONS] LENGTH\n");
        fprintf(stderr, "\n");
        /*
        printf("OPTIONS: \n");
        printf("-n | --no-symbole: only use alphanumerical characters\n");
        */
        return 1;
}

char get_random_char() {
    int ascii_value  = (33 + (random() % 93) + 1);
    return (char)ascii_value;
}
