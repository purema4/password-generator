# Passwordgen
Super simple password generator for linux wrote in C
_first time i code in C, its my first learning experience_

## Usage
```
$ ./bin/passwordgen LENGTH
```

## Someting special?

Yes! is uses the _getrandom_ linux syscall like this:

```c
unsigned long int *seed = malloc(sizeof(unsigned long int));
if(!seed) {
    fprintf(stderr, "unable to allocate buffer size space. exiting\n");
    exit(1);
}
getrandom(seed, sizeof(unsigned long int), 0);

//initializing random pool with buffer
srandom(*seed);
free(seed);
```
