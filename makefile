CFLAGS = -g -Wall -O3
TARGETS = passwordgen

all: clean passwordgen

passwordgen: passwordgen.c
	gcc $< $(CFLAGS) -o bin/$@

clean:
	rm -f bin/$(TARGETS)